import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        ReadInput inputReader = new ReadInput();
        Controller controller = new Controller();
        List<String> inputFileAsStringList;

        int day = controller.waitForInput();
        String pathname = "input"+day+".txt";

        inputFileAsStringList = inputReader.readInputFileToListe(new File(pathname));

        int result;
        switch (day) {
            case 1:
                result = controller.calculateFrequency(inputFileAsStringList);
                break;
            case 2:
                result = controller.calculateTwoAndThreeTimes(inputFileAsStringList);
                break;
            case 3:
                result = controller.countSquares((inputFileAsStringList));
                break;
            default:
                result = 0;
        }

        System.out.println("result: " + result);
    }



}
