import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadInput {

    File inputFile;
    List<String> inputFileAsStringList;

    public List<String> readInputFileToListe (File inputFile) {

        this.inputFile = inputFile;
        String text;

        inputFileAsStringList = new ArrayList<>();

        StringBuilder content = new StringBuilder();

        int index = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            while ((text = reader.readLine()) != null) {
                content.append(text);
                inputFileAsStringList.add(index, content.toString());
                content.delete(0, content.length());
                index++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return inputFileAsStringList;
    }
}

