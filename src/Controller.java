import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class Controller {

    int result;

    public void Controller() {

    }

    public int waitForInput() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.print("Challenge day: ");
        String eingabe = br.readLine();
        return parseInt(eingabe);
    }

    public int calculateFrequency(List<String> inputFileAsStringList) { // day1
        int result = 0;
        ArrayList<Integer> valueList = new ArrayList<>();

        boolean f = true;

        while (f) {
            for (int i = 0; i < inputFileAsStringList.size(); i++) {

                int value;
                char vorzeichen = inputFileAsStringList.get(i).charAt(0);
                if (vorzeichen == '-') {
                    value = parseInt(inputFileAsStringList.get(i).substring(1));
                    result -= value;
                } else {
                    value = parseInt(inputFileAsStringList.get(i));
                    result += value;
                }
                if (valueList.contains(result)) {
                    System.out.println("done");
                    f = false;
                    return result;
                } else valueList.add(result);
                //System.out.println(result);

            }
        }
        return result;
    }

    public int calculateTwoAndThreeTimes(List<String> inputFileAsStringList) { //day2

        int charTwoTimes = 0;
        int charThreeTimes = 0;

        for (int i = 0; i < inputFileAsStringList.size(); i++){
            boolean twice = false;
            boolean threetimes = false;

            HashMap<Character, Integer> charCountMap = new HashMap<>();
            char[] strArray = inputFileAsStringList.get(i).toCharArray();
            for (char c : strArray){
                if(charCountMap.containsKey(c)){
                    charCountMap.put(c, charCountMap.get(c)+1);
                }
                else
                    charCountMap.put(c, 1);
            }

            Set<Character> charsInString = charCountMap.keySet();
            for(Character ch : charsInString){
                if(charCountMap.get(ch) == 2 && !twice){
                    charTwoTimes++;
                    twice = true;
                }
                else if (charCountMap.get(ch) == 3 && !threetimes){
                    charThreeTimes++;
                    threetimes = true;
                }
            }

        }



        List<String> compairedString = new ArrayList<>();


        for(int i = 0; i<inputFileAsStringList.size(); i++){
            String[] s = inputFileAsStringList.get(i).split("");
            for (int j = i+1; j<inputFileAsStringList.size()-1; j++){
                 String resultStrings = new String("");
                String[] sCompare = inputFileAsStringList.get(j).split("");
                for (int k = 0; k<sCompare.length; k++){
                  //  System.out.println("sCompare[k].equals(s[k])" + sCompare[k] + "," + s[k]);
                    if (sCompare[k].equals(s[k])){
                        resultStrings = resultStrings.concat(s[k]);
                    }
                }
                compairedString.add(resultStrings);
             }
        }
        int longestValue=0;
        String resultString = new String("");
        for(String s: compairedString){
            if(s.length()>longestValue){
                longestValue=s.length();
                resultString=s;
            }
        }
        System.out.println("resultString : " + resultString);

        result = charThreeTimes * charThreeTimes;
        return result;
    }

    public int countSquares(List<String> inputFileAsStringList){    // day3
        int row[][] = new int[10000][10000];

        String claim = new String(" ");

        for(String s : inputFileAsStringList){
            String[] wholeLineString = s.replace(":", "").split(" ");
            String[] positionString = wholeLineString[2].split(",");
            String[] squareSizeString = wholeLineString[3].split("x");

            for (int i = 0; i< parseInt(squareSizeString[0]); i++){
              for (int j = 0; j< parseInt(squareSizeString[1]); j++){
                  int posX = parseInt(positionString[0]) + i;
                  int posY = parseInt(positionString[1]) + j;
                  row[posX][posY] ++;
              }
            }
        }

        for(int i = 0; i<10000; i++) {
            for (int j = 0; j < 10000; j++) {
                if(row[i][j]>=2){
                    result++;
                }
            }
        }

        for(String s : inputFileAsStringList){
            String[] wholeLineString = s.replace(":", "").split(" ");
            String[] positionString = wholeLineString[2].split(",");
            String[] squareSizeString = wholeLineString[3].split("x");
            boolean validclaim = true;
            for (int i = 0; i< parseInt(squareSizeString[0]); i++) {
                for (int j = 0; j < parseInt(squareSizeString[1]); j++) {
                    int posX = parseInt(positionString[0]) + i;
                    int posY = parseInt(positionString[1]) + j;

                    if (row[posX][posY] > 1) validclaim = false;
                }
            }
            if (validclaim) claim=wholeLineString[0];
        }

        System.out.println("Claim: " + claim);
        return result;
    }
}
